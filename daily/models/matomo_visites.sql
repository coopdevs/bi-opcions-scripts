
{{ config(materialized='table') }}

SELECT  v.idvisit, v.idvisitor, v.referer_url, a.name as current_url
,a1.name as page_name
,a2.name as previous_page_name
,a3.name as previous_url
,va.pageview_position, va.server_time
,SPLIT_PART(a.name, '/', 1) as sp1
,SPLIT_PART(a.name, '/', 2) as sp2
,SPLIT_PART(a.name, '/', 3) as sp3
,SPLIT_PART(a.name, '/', 4) as sp4
,SPLIT_PART(a.name, '/', 5) as sp5
,SPLIT_PART(a.name, '/', 6) as sp6
,SPLIT_PART(a.name, '/', 7) as sp7
,SPLIT_PART(a.name, '/', 8) as sp8
FROM matomo_db_matomo_log_visit v
LEFT JOIN matomo_db_matomo_log_link_visit_action va ON v.idvisit = va.idvisit
LEFT JOIN matomo_db_matomo_log_action a ON a.idaction = va.idaction_url
LEFT JOIN matomo_db_matomo_log_action a1 ON a1.idaction = va.idaction_name
LEFT JOIN matomo_db_matomo_log_action a2 ON a2.idaction = va.idaction_name_ref
LEFT JOIN matomo_db_matomo_log_action a3 ON a3.idaction = va.idaction_url_ref
where v.idsite=3
