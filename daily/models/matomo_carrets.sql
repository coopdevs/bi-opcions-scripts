
{{ config(materialized='table') }}

SELECT v.idvisit, v.idvisitor, v.referer_url
	,c.idorder conversion_idorder, c.server_time, c.items, c.revenue, c.revenue_shipping, c.revenue_subtotal, c.revenue_tax, c.revenue_discount, c.visitor_returning, c.visitor_count_visits
 	,ci.idorder conversionitem_idorder, ci.price, ci.quantity, ci.deleted
 	,acs.name  as ref
	, an.name as product_name, ac.name as product_cat_lvl1, ac2.name as product_cat_lvl2, ac3.name as product_cat_lvl3, ac4.name as product_cat_lvl4, ac5.name as product_cat_lvl5
FROM matomo_db_matomo_log_visit v
	JOIN matomo_db_matomo_log_conversion c ON v.idvisit = c.idvisit
	JOIN matomo_db_matomo_log_conversion_item ci ON v.idvisit = ci.idvisit
	left join matomo_db_matomo_log_action acs on acs.idaction =ci.idaction_sku
	left join matomo_db_matomo_log_action an on an.idaction =ci.idaction_name
	left join matomo_db_matomo_log_action ac on ac.idaction =ci.idaction_category
	left join matomo_db_matomo_log_action ac2 on ac2.idaction =ci.idaction_category2
	left join matomo_db_matomo_log_action ac3 on ac3.idaction =ci.idaction_category3
	left join matomo_db_matomo_log_action ac4 on ac4.idaction =ci.idaction_category4
	left join matomo_db_matomo_log_action ac5 on ac5.idaction =ci.idaction_category5
where v.idsite=3
